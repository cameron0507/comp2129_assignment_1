#! /usr/bin/env sh

echo "Testing begin:";

echo "Test case 1: User provides correct input:"
T1=$(./minesweeper < tests/correct.in | diff - tests/correct.out);
if [ "$T1" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T1\n";
fi

echo "Test case 2: User input blank line at grid init:"
T2=$(./minesweeper < tests/blankline-1.in | diff - tests/blankline-1.out);
if [ "$T2" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T2\n";
fi

echo "Test case 3: User input blank line at bomb init:"
T3=$(./minesweeper < tests/blankline_2.in | diff - tests/blankline_2.out);
if [ "$T3" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T3\n";
fi


echo "Test case 4: User input blank line at game time:"
T4=$(./minesweeper < tests/blankline_3.in | diff - tests/blankline_3.out);
if [ "$T4" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T4\n";
fi

echo "Test case 5: User flags too many cells:"
T5=$(./minesweeper < tests/too_many_flags.in | diff - tests/too_many_flags.out);
if [ "$T5" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T5\n";
fi

echo "Test case 6: User places too many mines:"
T6=$(./minesweeper < tests/too_many_mines.in | diff - tests/too_many_mines.out);
if [ "$T6" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T6\n";
fi

echo "Test case 7: User provides incorrect grid initialisation:"
T7=$(./minesweeper < tests/incorrect_grid_init.in | diff - tests/incorrect_grid_init.out);
if [ "$T7" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T7\n";
fi

echo "Test case 8: User provides incorrect bomb placement:"
T8=$(./minesweeper < tests/incorrect_bomb.in | diff - tests/incorrect_bomb.out);
if [ "$T8" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T8\n";
fi

echo "Test case 9: User provides incorrect grid guess at game time:"
T9=$(./minesweeper < tests/incorrect_grid_ref.in | diff - tests/incorrect_grid_ref.out);
if [ "$T9" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T9\n";
fi

echo "Test case 10: User flags a cell with no mine:"
T10=$(./minesweeper < tests/flag_nomine_cell.in | diff - tests/flag_nomine_cell.out);
if [ "$T10" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T10\n";
fi


echo "Test case 11: User provides same command twice at bomb initialisation:"
T11=$(./minesweeper < tests/same_command_twice_bomb_init.in | diff - tests/same_command_twice_bomb_init.out);
if [ "$T11" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T11\n";
fi

echo "Test case 12: User provides random input at grid initialisation:"
T12=$(./minesweeper < tests/rand_grid_init.in | diff - tests/rand_grid_init.out);
if [ "$T12" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T12\n";
fi

echo "Test case 13: User provides random input at bomb initialisation:"
T13=$(./minesweeper < tests/rand_bomb_init.in | diff - tests/rand_bomb_init.out);
if [ "$T13" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T13\n";
fi

echo "Test case 14: User provides random input at game time:"
T14=$(./minesweeper < tests/rand_gametime.in | diff - tests/rand_gametime.out);
if [ "$T14" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T14\n";
fi

echo "Test case 15: User provides same command twice at game time:"
T15=$(./minesweeper < tests/same_command_twice_gametime.in | diff - tests/same_command_twice_gametime.out);
if [ "$T15" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T15\n";
fi

echo "Test case 16: Testing for a non-square grid layout:"
T16=$(./minesweeper < tests/nonsquare_grid.in | diff - tests/nonsquare_grid.out);
if [ "$T16" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T16\n";
fi


echo "Test case 17: User inputs whitespace after valid command:"
T17=$(./minesweeper < tests/whitespace.in | diff - tests/whitespace.out);
if [ "$T17" = "" ]
then
	echo "Passed."
else
	echo "Failed. diff output: ";
    echo "$T17\n";
fi

echo "Testing end.";
