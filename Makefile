#! /usr/bin/env sh

hellomake: minesweeper.c
	clang -std=c11 -Wall minesweeper.c -o minesweeper
