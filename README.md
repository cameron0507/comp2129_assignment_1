# README #

COMP2129 Assignment 1 2016: Minesweeper.

### What is this repository for? ###

* This is a git repository for my work on my first COMP2129 assignment, in which I have to develop a basic minesweeper clone in C.
* Version: 0.0.1.

### How do I get set up? ###

```
Sample input
In the following test case we construct a 4 x 4 mine field and then play the game.
The player successfully flags all of the mines and uncovers the remaining cells in this game.
g 4 4
b 0 0
b 1 0
b 3 0
b 1 1
b 1 2
b 0 2
b 3 2
b 0 3
b 1 3
b 3 3
u 2 3
u 2 0
f 0 3
u 2 1
f 3 0
f 0 2
u 0 1
f 3 2
f 3 3
f 0 0
f 1 1
f 1 0
f 1 2
u 3 1
u 2 2
f 1 3

Displaying the grid
The grid is represented using 1 character per cell, with a 1 character border. A 4 × 4 grid looks like:
+----+
|****|
|****|
|****|
|****|
+----+
• The border consists of - or | characters, with a + character in each corner.
• The * character represents a covered cell.
• The f character represents a flagged cell.
• An uncovered cell is represented by a number that corresponds to the total number of bombs in
the 8 adjacent cells that surround the uncovered cell.
The top left corner of the grid should have coordinates (0, 0) and the bottom right corner of the grid
should have coordinates (<width> − 1, <height> − 1). The first coordinate is the x coordinate,
and the second is the y coordinate. A 4 × 4 grid where the player has flagged (3, 1) and uncovered
(0, 2) with 2 mines in the adjacent cells would look like:
+----+
|****|
|***f|
|2***|
|****|
+----+
```

### Contribution guidelines ###

* This is a personal assignment - contributions by anyone but the owner are not permitted. Additionally, viewing of this repository by others currently enrolled in COMP2129 is not permitted.

### Who do I talk to? ###

* Repo owner: Cameron Nichols
* [Email](cnic3139@uni.sydney.edu.au)