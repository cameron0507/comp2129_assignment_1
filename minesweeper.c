/*
 *  * comp2129 - assignment 1
 *  * Author: Cameron Nichols.
 *  * Unikey: cnic3139.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Declaring struct "Cell"; the grid will be a 2D array of Cells.
struct Cell {

	int has_bomb;   // Boolean.
	int flagged;    // Boolean.
	int covered;    // Boolean.
	int nearby;     // Integer.

};

// Declaring global variables for the width & height of the grid.
int width, height;

// Counter for # of flags placed (max 10).
int flag_count = 0;

// Char used in checking validity of input.
char newline;

// Error function, for erroneous input
void error_function() {
	printf("error\n");
	exit(0);
}

// Bomb setup/initialisaion method
void bomb_init(struct Cell grid[width][height]) {

	// 10 bombs are placed on the grid, designated by a 'b'
	for (int i = 0; i < 10; i++) {

		char input;
		int bomb_width, bomb_height;
		// Expect user input to be of the form 'b <x> <y>'

		scanf("%c", &input);
		if (input != 'b') {
			error_function();
		}
		scanf("%d", &bomb_width);
		scanf("%d", &bomb_height);

		// ensure newline was scanned
		while(1) {
			scanf("%c", &newline);
			if (newline == ' ') {
				continue;
			}
			if (newline != '\n') {
				error_function();
			} else {
				break;
			}
		}

		// If placing bomb where bomb already is, throw error
		if(grid[bomb_width][bomb_height].has_bomb){
			error_function();
		}

		// Placing the bomb in designated grid tile if in range
		if (bomb_width < width && bomb_height < height) {
			grid[bomb_width][bomb_height].has_bomb = 1;
		}

		// Print user command to the screen.
		printf("%c %d %d\n", input, bomb_width, bomb_height);

	}

	// Calculating bombs in nearby cells.
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			// If tile has bomb, skip.
			if (!grid[j][i].has_bomb) {
				// Check left of tile is valid.
				if (j - 1 >= 0) {
					// Then immediate left.
					if (grid[j-1][i].has_bomb) {
						grid[j][i].nearby++;
					}
					// Check left-above.
					if (i - 1 >= 0 && grid[j-1][i-1].has_bomb) {
						grid[j][i].nearby++;
					}
					// Check left-below.
					if (i + 1 < height && grid[j-1][i+1].has_bomb) {
						grid[j][i].nearby++;
					}
				}
				// Check immediate above.
				if (i - 1 >= 0 && grid[j][i-1].has_bomb) {
					grid[j][i].nearby++;
				}
				// Check right of tile is valid.
				if (j + 1 < width) {
					// Then immediate right.
					if (grid[j+1][i].has_bomb) {
						grid[j][i].nearby++;
					}
					// Check right-above.
					if (i - 1 >= 0 && grid[j+1][i-1].has_bomb) {
						grid[j][i].nearby++;
					}
					// Check right-below.
					if (i + 1 < height && grid[j+1][i+1].has_bomb) {
						grid[j][i].nearby++;
					}
				}
				// Check immediate below.
				if (i + 1 < height && grid[j][i+1].has_bomb) {
					grid[j][i].nearby++;
				}
			}
		}
	}

}

// Grid update method (on user input)
void grid_update(struct Cell grid[width][height],
				 char uchar,
				 int uwidth,
				 int uheight) {

	if (uchar == 'u') {
		// If player uncovers a bomb, they lose the game
		if (grid[uwidth][uheight].has_bomb) {
			printf("lost\n");
			exit(0);
		}
		// If uncovered, same command has been entered twice, therefore error.
		if(!grid[uwidth][uheight].covered) {
			error_function();
		}
		grid[uwidth][uheight].covered = 0;
	}
	if (uchar == 'f') {
		if (flag_count == 10) {
			error_function();
		}
		flag_count++;
		grid[uwidth][uheight].flagged = 1;
	}

}

// Funciton to print grid to the display
void print_grid(struct Cell grid[width][height]) {

	// Printing top line
	printf("+");
	for (int i = 0; i < width; i++) {
		printf("-");
	}
	printf("+\n");

	// Printing the body of the grid
	for (int i = 0; i < height; i++) {
		printf("|");
		for (int j = 0; j < width; j++) {
			// Look at cell, determine if flagged or not
			if (grid[j][i].flagged) {
				printf("f");
				continue;
			}
			if (!grid[j][i].covered) {
				// Print # bombs in square around cell.
				printf("%d", grid[j][i].nearby);
				continue;
			}
			printf("*");
		}
		printf("|\n");
	}

	// Printing the bottom line
	printf("+");
	for (int i = 0; i < width; i++) {
		printf("-");
	}
	printf("+\n");

}

// Main method
int main(void) {

	// Variable for user input.
	char input[2];

	// Scan user input, expect a "g"
	scanf("%s", &input[0]);
	if (strcmp("g", input)) {
		error_function();
	}

	// Input has a g, now scan next input to get grid width & height.
	scanf("%d", &width);
	scanf("%d", &height);

	// Checking grid has at least 10 squares
	if (width * height < 10) {
		error_function();
	}

	// ensure newline was scanned
	while(1) {
		scanf("%c", &newline);
		if (newline == ' ') {
			continue;
		}
		if (newline != '\n') {
			error_function();
		} else {
			break;
		}
	}

	// Print user command to the screen.
	printf("%s %d %d\n", input, width, height);

	// Declaration and Initialisation of the grid.
	struct Cell grid[width][height];
	// Initalising all cells in grid
	for (int i = 0; i < height; i ++) {
		for (int j = 0; j < width ; j++) {
			grid[j][i].has_bomb = 0;
			grid[j][i].flagged = 0;
			grid[j][i].covered = 1;
			grid[j][i].nearby = 0;
		}
	}

	// Placement of the bombs, handled by seperate funciton.
	bomb_init(grid);

	// Grid created, bombs laid. Grid must now be drawn.
	print_grid(grid);

	// Game begins, the maximum number of terms is the number of cells
	for (int i = 1; i <= height * width; i++) {
		char uchar;
		int uwidth, uheight;



		scanf("%c", &uchar);
		if (uchar != 'f' && uchar != 'u') {
			error_function();
		}
		scanf("%d", &uwidth);
		scanf("%d", &uheight);

		// ensure newline was scanned
		while(1) {
			scanf("%c", &newline);
			if (newline == ' ') {
				continue;
			}
			if (newline != '\n') {
				error_function();
			} else {
				break;
			}
		}

		// Checking user has input a cell inside the grid
		if (uwidth > width || uheight > height) {
			error_function();
		}

		grid_update(grid, uchar, uwidth, uheight);

		// Print user command to the screen.
		printf("%c %d %d\n", uchar, uwidth, uheight);

		print_grid(grid);
	}

	// If this stage has been reached,  the game has been completed
	// 	successfully, the player has won.
	printf("won\n");
	return 0; // Exit the program.

}
